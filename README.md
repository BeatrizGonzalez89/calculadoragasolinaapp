## CalculadoraGasolinaApp

Esta aplicación ha sido desarrollada por Beatriz González tomando como base codigo en link https://github.com/misaelnieto/CalculadoraGasolinazo,
las herramientas que se utilizaron son las siguientes:

1. Java, Android nativo.
2. XML (Diseño de layout).
3. Android Studio.
4. Gestor de base de datos SQLite.

Descripción de aplicación: Compara el precio de gasolina entre dos paises.
Convertidor Dólares/Galón a Pesos/Litro y viceversa.

